# Program written by Steeven Brunner in april 2017

#Basic size of each room is 1 * 1
class Room
  def initialize (x, y)
    @x = x
    @y = y


    @walls_doors = Array.new(4)
    #north
    @walls_doors[0] =
    #east
    @walls_doors[1] =
    #south
    @walls_doors[2] =
    #west
    @walls_doors[3] =

    #Assignate a monster randomly (with 33% chance here) to the room
    @monster = false
    nb = rand(3)
    if nb == 1
      @monster = true
    end
  end

  def get_walls_doors
    return @walls_doors
  end

  #type must be either 'wall' or 'door'
  def set_walls_doors (position, type)
    if position >= 0 && position <= 3
      if type == 'door'
        @walls_doors[position]
      end

    end

  end

end

# Each room placed at any corner of the maze should contains a door