# Program written by Steeven Brunner in april 2017

require './door'
require './maze'
require './room'
require './wall'

class MazeFactory

  def make_maze(length, height)
   return Maze.new(length, height)
  end

  def make_room(x, y)
    return Room.new(x, y)
  end

  #r1 = room1, r2 = room 2
  def make_door(r1, r2)
    return Door.new(r1, r2)
  end

  #r1 = room1, r2 = room 2
  def make_wall(r1, r2)
    return Wall.new(r1, r2)
  end
end

def make_maze(x_size, y_size)
  factory = MazeFactory.new()
  m = factory.make_maze(x_size, y_size)
end

# x_size and y_size correspond to the number of rooms that the maze contains
make_maze(7, 7)

# Once the player enters in a room he can decide to L (leave) or F (fight) if a monster is in the room.
# If he decides to L, he has to choose which door he wants to pass through N (north), E (east), S (south), W (west).