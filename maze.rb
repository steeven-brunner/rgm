# Program written by Steeven Brunner in april 2017

class Maze

  def initialize (length, height)
    @length = length
    @height = height

    # 2D array containing the rooms
    @theMap = Array.new(@length) {Array.new(@height)}

    puts "\nDimensions du labyrinthe" #to remove
    puts@length #to remove
    puts @height #to remove
    puts #to remove

    #abort("Debug 1") #to remove

    game_loop()
    puts "\nEnd of MAZE"

  end

  def game_loop
    print 'Through which gate do you want to enter the maze? (N, E, S, W): '
    entry = gets.chomp()
    print "\nYou enter a room.\n"


    #@theMap[@length / 2][0] = Room.new(@length / 2, 0)

    current_x = 0
    current_y = 0

    if entry == 'N'
      current_x = @length / 2
      current_y = 0
      puts 'North'
      if @theMap[current_x][current_y] == nil
        puts 'La pièce dans laquelle le joueur rentre n\'existe pas'
        Room.new(current_x, current_y)
      end
    elsif entry == 'E'
      current_x = @length - 1
      current_y = @height / 2
      if @theMap[current_x][current_y] == nil
        puts 'La pièce dans laquelle le joueur rentre n\'existe pas'
        Room.new(current_x, current_y)
      end
    elsif entry == 'S'
      current_x = @length / 2
      current_y = @height - 1
      if @theMap[current_x][current_y] == nil
        puts 'La pièce dans laquelle le joueur rentre n\'existe pas'
        Room.new(current_x, current_y)
      end
    elsif entry == 'W'
      current_x = 0
      current_y = @height / 2
      if @theMap[current_x][current_y] == nil
        puts 'La pièce dans laquelle le joueur rentre n\'existe pas'
        Room.new(current_x, current_y)
      end
    end
    puts "position du joueur : #{current_x}  \\  #{current_y}"


    #
    # vérifier les pièces voisines
    #
    if @theMap[current_x + 1][current_y] == nil
      #puts 'La pièce à droite n\'existe pas'
    else
      puts 'La pièce à droite existe deja'
      #@theMap[current_x + 1][current_y]
    end


    if @theMap[current_x][current_y + 1] == nil
      #puts 'La pièce en haut n\'existe pas'
    else
      puts 'La pièce en haut existe deja'
      #@theMap[current_x][current_y + 1]
    end


    if @theMap[current_x - 1][current_y] == nil
      #puts 'La pièce à gauche n\'existe pas'
    else
      puts 'La pièce à gauche existe deja'
      #@theMap[current_x - 1][current_y]
    end


    if  @theMap[current_x][current_y - 1] == nil
      #puts 'La pièce en bas n\'existe pas'
    else
      puts 'La pièce en bas existe deja'
      #@theMap[current_x][current_y - 1]
    end


  end

end